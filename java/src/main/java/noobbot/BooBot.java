package noobbot;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hungpt2
 */
public class BooBot{

    final Gson gson = new Gson();
    private PrintWriter writer;
    public BooBot(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException 
    {
	this.writer = writer;
        String line = null;

        send(join);
	
        while((line = reader.readLine()) != null) 
	{
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                send(new Throttle(0.5));
		System.out.println("Throllle");
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
		System.out.println("not handled: " + gson.toJson(msgFromServer));
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
		System.out.println("Start: " + gson.toJson(msgFromServer));
            }
	    else if (msgFromServer.msgType.equals("yourCar")) {
                System.out.println("Ok my car is: " + gson.toJson(msgFromServer));
		
            }
	    else {
                send(new Ping());
		System.out.println("not handled: " + gson.toJson(msgFromServer));
            }
        }
	
    }
    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    public boolean UpdateRace(MsgWrapper Msg)
    {
	return false;
    }
    public static void Log(Object o)
    {
	System.out.println(o.toString());
    }
}
